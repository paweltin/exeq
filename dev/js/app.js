window.$ = window.jQuery = require('jquery');
require('../../bower_components/parallax/deploy/jquery.parallax');
require('../../node_modules/jquery.scrollto/jquery.scrollTo');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/scrollspy');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/popover');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/button');
window.ScrollMagic = require('../../node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic');

require('./widgets/join-us').init();
require('./widgets/gallery');
require('./widgets/counter');
require('./widgets/map-list');
require('./widgets/svg-list');

$(window).on('load', function() {
  setTimeout(function() {
    $('h1, h2').filter(':not(.header-regular)').addClass('header-fancy');
  }, 1000);

  setTimeout(function() {
    if (window.location.hash) {
      $(window).scrollTo(window.location.hash);
    }
  }, 0);
});

$(document).ready(function() {
  if ($('.visible-lg').is(':visible')) {
    $('.navbar-toggle').on('click', function(e) {
      e.stopImmediatePropagation();
    });
  }

  $('.navbar-toggle').on('mouseenter', function() {
    $('.navbar-collapse').collapse('show');
  });

  $('.navbar-collapse').on('mouseleave', function() {
    $('.navbar-collapse').collapse('hide');
  });

  var $logo = $('.logo');
  var src = $logo.data('src');

  $logo.on('load', function() {
    $('.logo, .header, .content').addClass('active2');
    $('.dot__container').fadeOut();

    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: 0.5, reverse: false}});
    new ScrollMagic.Scene({triggerElement: "#intro"})
      .setClassToggle(".intro-web", "active")
      .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#services"})
      .setClassToggle("#services", "active")
      .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#process"})
      .setClassToggle("#process", "active")
      .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#why-us"})
      .setClassToggle("#why-us", "active")
      .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#clients-gallery"})
      .setClassToggle("#clients-gallery", "active")
      .addTo(controller)
      .triggerHook(0.8);
    new ScrollMagic.Scene({triggerElement: "#staff"})
      .setClassToggle("#staff", "active")
      .addTo(controller)
      .triggerHook(0.8);
  }).attr('src', src);

  $('.map-list').each(function(i) {
    $(this).mapList();
  });

  $('.svg-list').each(function(i) {
    $(this).svgList();
  });

  $('.gallery').each(function() {
    $(this).gallery();
  });

  $('.counter').each(function(i) {
    $(this).counter(i);
  });

  $('a[href^="#"]').on('click', function(e) {
    e.preventDefault();

    var id = $(this).attr('href');

    if (id.length == 1) {
        return;
    }

    $(window).scrollTo(id, {
      duration: 400,
      onAfter: function() {
        window.location.hash = id;
      }
    });
  });

  $('.content').on('click', function() {
    if ($(this).hasClass('collapsed')) {
      $('.navbar-toggle').click();
    }
  });

  $('.gallery-caption-desc a:first-of-type').before('<br>');
  $('.gallery-caption-desc a').attr('target', '_blank');

  $('.scene').parallax().find('.layer div').css('opacity', 1);

  $('#mc4wp-form-1').on('submit', function(e) {
    e.preventDefault();

    var that = this;
    $.ajax({
      data: $(that).serialize(),
      method: 'post',
      dataType: 'html',
      beforeSend: function() {
        $('.mc4wp-response').fadeOut(function() {
          $(this).remove();
        });
      },
      complete: function(response) {
        var $alert = $('<div></div>').append($.parseHTML(response.responseText)).find('.mc4wp-response');

        $(that).append($alert).hide().fadeIn();
      }
    })
  });
});

$(function () {
    $('[data-toggle="popover"]').popover();

    var nav = '.scrollspy';
    var content = '.content';
    var logo = '.logo__container';
    moveNav(nav);
    $('#main-menu').on('hidden.bs.collapse', function () {
        moveNav(nav);
    }).on('hide.bs.collapse', function() {
        moveContent([content, logo]);
    }).on('show.bs.collapse', function () {
        moveNav(nav);
        moveContent([content, logo]);
    });
});

function moveNav(nav) {
    if ($('.header').children(nav).length) {
        $(nav).removeClass('fade-in').insertAfter('.languages');
    } else {
        $(nav).detach().appendTo('.header');

        setTimeout(function() {
            $(nav).addClass('fade-in');
        }, 0);
    }
}

function moveContent(elementArray) {
  for (var i in elementArray) {
    $(elementArray[i]).toggleClass('collapsed');
  }
}
