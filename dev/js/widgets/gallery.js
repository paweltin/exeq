$.fn.extend({
  gallery: function() {
    this.options = $(this).parents('.gallery__container').data();
    this.$items = $(this).find('.gallery-item');
    this.$prev = $(this).parents('.gallery__container').find('.gallery-nav__prev');
    this.$next = $(this).parents('.gallery__container').find('.gallery-nav__next');
    this.activeItem = 0;

    this.device = (function() {
      var documentWidth = $(document).width();
      return documentWidth <= 1490 ? (documentWidth <= 1024 ? (documentWidth <= 768 ? (documentWidth <= 480 ? 4 : 3) : 2) : 1) : 0;
    })();

    if (!this.options.itemWidth[this.device]) {
      return undefined;
    }

    var galleryWidth = this.options.itemWidth[this.device] * this.$items.length;
    $(this).css('width', galleryWidth);

    this.galleryPrev = function(e) {
      e = e || new Event('click');
      e.preventDefault();
      if (interval) {
        this.resetAutoplay();
      }

      if (this.options.loop) {
        var transitionValue = $(this).css('transition');
        this.activeItem++;

        $(this).find('.gallery-item').eq(this.$items.length - 1).detach().prependTo(this);
        $(this).css('transition', 'none');
        $(this).css('transform', 'translateX(-' + this.options.itemWidth[this.device] * this.activeItem + 'px)');

        setTimeout((function() {
          $(this).css('transition', transitionValue);
        }).bind(this), 10);
      }

      if (this.getPrevIndex() >= 0) {
        setTimeout((function() {
          this.activeItem--;
          var translateString = 'translateX(-' + this.options.itemWidth[this.device] * this.activeItem + 'px)';
          $(this).css('transform', translateString);
          $(this).parents('.gallery__container').find('.gallery__additional').css('transform', translateString);
        }).bind(this), 15);
      }
    };

    this.galleryNext = function(e) {
      e = e || new Event('click');
      e.preventDefault();
      if (interval) {
        this.resetAutoplay();
      }

      if (this.getNextIndex() <= this.$items.length - this.options.itemsVisible[this.device]) {
        this.activeItem++;
        var translateString = 'translateX(-' + this.options.itemWidth[this.device] * this.activeItem + 'px)';
        $(this).css('transform', translateString);
        $(this).parents('.gallery__container').find('.gallery__additional').css('transform', translateString);
      }

      if (this.options.loop) {
        var transitionValue = $(this).css('transition');
        this.activeItem--;

        setTimeout((function() {
          $(this).find('.gallery-item').eq(this.activeItem).detach().appendTo(this);
          $(this).css('transition', 'none');
          $(this).css('transform', 'translateX(0)');

          setTimeout((function() {
            $(this).css('transition', transitionValue);
          }).bind(this), 50);
        }).bind(this), 300);
      }
    };

    this.$prev.on('click', this.galleryPrev.bind(this));
    this.$next.on('click', this.galleryNext.bind(this));

    this.getPrevIndex = function() {
      return this.activeItem - 1;
    };

    this.getNextIndex = function() {
      return this.activeItem + 1;
    };

    var interval = null;
    this.resetAutoplay = (function() {
      clearInterval(interval);
      interval = setInterval((function() {
        this.galleryNext();
      }).bind(this), 5000);
    }).bind(this);

    this.$items.each(function(i) {
      $(this).css('transition-delay', .2*(i+2) + 's');
    });

    setTimeout((function() {
      var imageHeight = this.$items.find('img:first').height();
      this.$items.filter('.gallery-item--link').find('a').css('line-height', imageHeight + 'px');
    }).bind(this), 1000);

    if (this.options.autoplay) {
      this.resetAutoplay();
    }
  }
});
