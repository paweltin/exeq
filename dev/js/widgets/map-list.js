$.fn.extend({
  mapList: function() {
    this.$mapList = $(this);
    this.$mapListItemTemplate = this.$mapList.find('li:first');
    this.$mapListItemFirstTemplate = this.$mapList.find('li:nth-child(2)');
    this.$mapListItemLastTemplate = this.$mapList.find('li:last');
    this.$listToMap = this.$mapList.next('.list-to-map');
    this.gallery = false;

    if ($(this).hasClass('gallery')) {
      this.gallery = true;
      $(this).removeClass('gallery');
    }

    this.componentName = $(this).removeClass('map-list').attr('class');

    if (!this.$listToMap.length) {
      return undefined;
    }

    this.$mapList.html('');
    var that = this;
    this.$listToMap.find('li').each(function(i) {
      var last = i == that.$listToMap.find('li').length - 1;
      var $itemTemplate = that.$mapListItemTemplate.clone();

      if (that.$mapListItemFirstTemplate.length && !i) {
        $itemTemplate = that.$mapListItemFirstTemplate.clone();
      }

      if (last) {
        $itemTemplate = that.$mapListItemLastTemplate.clone();
      }

      $itemTemplate.find('.' + that.componentName + '__desc').html($(this).find('b').html());
      $(this).find('b').remove();
      $itemTemplate.find('[data-toggle=popover]').data('content', $(this).text());

      if (!(i % 3)) {
        $itemTemplate.addClass(that.componentName + '__item--left');
      } else if (!((i - 2) % 3)) {
        $itemTemplate.addClass(that.componentName + '__item--right');
      }

      if (!i || last) {
        $itemTemplate.addClass(that.componentName + '__item--final')
      }

      that.$mapList.append($itemTemplate);
    });

    $(this).addClass('map-list');
    if (this.gallery ) {
      $(this).addClass('gallery');
    }

    $(this).find('a').on('click', function(e) {
      e.preventDefault();
    })
  }
});
