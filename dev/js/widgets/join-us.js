module.exports = {
  init: function() {
    this.$list = $('#staff').find('.gallery');
    this.$item = $('.gallery-item.hidden').clone().removeClass('hidden');

    this.$list.append(this.$item);
  }
};
