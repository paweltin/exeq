$.fn.extend({
  svgList: function() {
    this.$svgList = $(this);
    this.$mapList = $(this).siblings('.map-list');

    var that = this;
    this.$svgList.find('li').each(function(i) {
      if (that.$mapList.hasClass('gallery')) {
        this.gallery = true;
        that.$mapList.removeClass('gallery');
      }

      var className = that.$mapList.removeClass('map-list').attr('class') + '__item--' + $(this).attr('class');
      that.$mapList.find('li').eq(i).addClass(className).find('a').prepend($(this).html());

      if (this.gallery ) {
        that.$mapList.addClass('gallery');
      }
    });
  }
});
