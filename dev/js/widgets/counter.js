$.fn.extend({
  counter: function(i) {
    var limit = parseInt($(this).text());
    var suffix = $(this).text().replace(/[0-9]/g, '');
    var $container = $('<div>').addClass('counter');
    var $list = $('<ul>').addClass('counter__list').css({
      'animation-timing-function': 'steps(' + limit + ', end)',
      'animation-delay': ((i + 1) * .2).toFixed(1) + 's'
    });

    for (var i = 0; i <= limit; i++) {
      var $item = $('<li>').addClass('counter__item').html(i + suffix);
      $item.appendTo($list);
    }

    $list.appendTo($container);
    $(this).replaceWith($container);
  }
});
