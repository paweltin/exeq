window.$ = window.jQuery = require('jquery');
require('../../bower_components/parallax/deploy/jquery.parallax');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/popover');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/button');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal');
window.ScrollMagic = require('../../node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic');

$(window).on('load', function() {
  setTimeout(function() {
    $('h1, h2').addClass('header-fancy');
  }, 1000);
});

$(document).ready(function() {
  if ($('.visible-md').is(':visible')) {
    $('.navbar-toggle').on('click', function(e) {
      e.stopImmediatePropagation();
    });
  }

  $('.navbar-toggle').on('mouseenter', function() {
    $('.navbar-collapse').collapse('show');
  });

  $('.navbar-collapse').on('mouseleave', function() {
    $('.navbar-collapse').collapse('hide');
  });

  var $logo = $('.logo');
  var src = $logo.data('src');

  $logo.on('load', function() {
    $('.logo, .header, .content').addClass('active2');
    $('.dot__container').fadeOut();

    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: 0.5, reverse: false}});
    new ScrollMagic.Scene({triggerElement: "#intro"})
      .setClassToggle(".intro-web", "active")
      .addTo(controller);
  }).attr('src', src);

  $('.content').on('click', function() {
    if ($(this).hasClass('collapsed')) {
      $('.navbar-toggle').click();
    }
  });

  $('.modal').each(function() {
    $(this).remove().appendTo('body');
  });

  $('.scene').parallax().find('.layer div').css('opacity', 1);

  $('#mc4wp-form-1').on('submit', function(e) {
    e.preventDefault();

    var that = this;
    $.ajax({
      data: $(that).serialize(),
      method: 'post',
      dataType: 'html',
      beforeSend: function() {
        $('.mc4wp-response').fadeOut(function() {
          $(this).remove();
        });
      },
      complete: function(response) {
        var $alert = $('<div></div>').append($.parseHTML(response.responseText)).find('.mc4wp-response');

        $(that).append($alert).hide().fadeIn();
      }
    })
  });

  initPopupListStyle();
});

$(function () {
  $('[data-toggle="popover"]').popover();

  var nav = '.scrollspy';
  var content = '.content';
  var logo = '.logo__container';
  moveNav(nav);
  $('#main-menu').on('hidden.bs.collapse', function () {
    moveNav(nav);
  }).on('hide.bs.collapse', function() {
    moveContent([content, logo]);
  }).on('show.bs.collapse', function () {
    moveNav(nav);
    moveContent([content, logo]);
  });
});

function moveNav(nav) {
  if ($('.header').children(nav).length) {
    $(nav).removeClass('fade-in').insertAfter('.languages');
  } else {
    $(nav).detach().appendTo('.header');

    setTimeout(function() {
      $(nav).addClass('fade-in');
    }, 0);
  }
}

function moveContent(elementArray) {
  for (var i in elementArray) {
    $(elementArray[i]).toggleClass('collapsed');
  }
}

function initPopupListStyle() {
  var $icon = $('svg.inactive').first();

  $('.modal-content li').each(function() {
    var $_icon = $icon.clone();
    $(this).prepend($_icon.clone());
  });
}
