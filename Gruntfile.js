module.exports = function(grunt) {
  grunt.initConfig({
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            '**/*.css',
            'wp-content/themes/exeq/index.js',
            '**/*.php'
          ]
        },
        options: {
          open: false,
          proxy: 'http://exeq.local',
          reloadOnRestart: true,
          ui: {
            port: 2999
          },
          watchTask: true
        }
      }
    },
    browserify: {
      dist: {
        files: {
          'wp-content/themes/exeq/index.js': ['dev/js/app.js'],
          'wp-content/themes/exeq/content.js': ['dev/js/app-content.js']
        }
      }
    },
    postcss: {
      dev: {
        options: {
          map: {
            inline: true
          },
          processors: [
            require('autoprefixer')
          ]
        },
        src: 'wp-content/themes/exeq/style.css'
      },
      dist: {
        options: {
          map: true,
          processors: [
            require('autoprefixer'),
            require('cssnano')(),
            require('pixrem')(),
            require('postcss-color-rgba-fallback')(),
            require('postcss-opacity')(),
            require('postcss-pseudoelements')(),
            require('postcss-vmin')()
          ]
        },
        src: 'wp-content/themes/exeq/style.css'
      }
    },
    sass: {
      options: {
        sourceMap: true,
        includePaths: [
          'bower_components',
          'node_modules',
          'node_modules/bootstrap-sass/assets/stylesheets'
        ]
      },
      dist: {
        files: {
          'wp-content/themes/exeq/style.css': 'dev/scss/style.scss'
        }
      }
    },
    uglify: {
      dist: {
        mangle: false,
        beautiy: true,
        files: {
          'wp-content/themes/exeq/index.js': ['wp-content/themes/exeq/index.js'],
          'wp-content/themes/exeq/content.js': ['wp-content/themes/exeq/content.js']
        }
      }
    },
    uncss: {
      dist: {
        options: {
          ignore: [
            '.open > .dropdown-menu',
            /navbar-nav/,
            /navbar-default/,
            /carousel-inner/,
            /modal/,
            /fade/,
            /in/,
            /-effect/,
            /hashtags-item/,
            /jumbotron--404/
          ]
        },
        files: {
          'wp-content/themes/exeq/style.css': ['wp-content/themes/exeq/index.php']
        }
      }
    },
    watch: {
      options: {
        atBegin: true,
        interrupt: false,
        livereload: true,
        spawn: false
      },
      browserify: {
        files: ['dev/**/*.js'],
        tasks: ['browserify']
      },
      sass: {
        files: ['dev/**/*.scss'],
        tasks: ['sass', 'postcss:dev']
      }
    }
  });

  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['sass:dist', 'postcss:dist', 'browserify:dist']);
  grunt.registerTask('full', ['imagemin', 'default']);
  grunt.registerTask('sync', ['browserSync', 'watch']);
};
