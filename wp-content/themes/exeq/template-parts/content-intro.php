<div class="content active2">
    <div class="intro__container scene">

        <div class="container">
            <div class="layer" data-depth="0.30">
                <div class="jobs__bg jobs__bg--1"></div>
            </div>

            <div class="layer" data-depth="0.10">
                <div class="jobs__bg jobs__bg--2"></div>
            </div>

            <section id="intro" class="section intro intro-web intro--content">
                <?php get_page_custom('Dołącz do nas'); ?>
            </section>

            <div class="svg-bg">
                <svg>
                    <linearGradient id="gradient-step" gradientUnits="userSpaceOnUse" x1="965.375" y1="1131.5695" x2="965.375" y2="1062.6805" gradientTransform="matrix(0.5 0 0 0.5 -462.3125 -528.4375)">
                        <stop offset="0" style="stop-color:#D72D2F"/>
                        <stop offset="0.704" style="stop-color:#d02c30"/>
                        <stop offset="1" style="stop-color:#902237"/>
                    </linearGradient>
                </svg>
            </div>

            <ul class="job-list">
