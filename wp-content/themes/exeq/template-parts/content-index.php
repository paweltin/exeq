<div class="content<?php global $visited; $visited ? print ' active2': null;?>">
    <div class="intro__container">

        <div class="container">
            <section id="intro" class="section intro intro-web">
                <?php get_page_custom('Intro'); ?>

                <a href="#services" class="read-more">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         viewBox="14 4 41 66" xml:space="preserve">
                        <style type="text/css">
                            #btn{fill:url(#gradient);}
                        </style>
                        <linearGradient id="gradient" gradientUnits="userSpaceOnUse" x1="34.5" y1="69.5" x2="34.5" y2="4.5">
                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                            <stop offset="1" style="stop-color:#8D263C"/>
                        </linearGradient>
                        <path id="btn" d="M34.5,4.5L34.5,4.5c11,0,20,9,20,20v25c0,11-9,20-20,20l0,0c-11,0-20-9-20-20v-25C14.5,13.5,23.5,4.5,34.5,4.5z"/>
                        <g id="arrow">
                            <path d="M9.2,7c0.4,0.4,0.4,1,0,1.4L5.7,12c0,0,0,0,0,0c-0.2,0.2-0.6,0.3-0.9,0.2c0.2,0,0.4-0.1,0.5-0.3c0.3-0.3,0.4-0.8,0.2-1.2c0.2-0.1,0.3-0.2,0.3-0.3V9.1L7.8,7C8.2,6.6,8.8,6.6,9.2,7z"/>
                            <path d="M5.8,9.1v1.4c0,0.1-0.1,0.2-0.3,0.3c0-0.1-0.1-0.2-0.2-0.3l-0.5-0.5L5.8,9.1z"/>
                            <path d="M5.8,1v8.1l-1,1l-1-1V1c0-0.6,0.4-1,1-1S5.8,0.4,5.8,1z"/>
                            <path d="M5.4,10.8c0.2,0.4,0.1,0.8-0.2,1.2c-0.1,0.1-0.3,0.2-0.5,0.3c-0.2,0-0.3-0.1-0.5-0.3c-0.3-0.3-0.4-0.8-0.2-1.2C4.5,10.9,5,10.9,5.4,10.8z"/>
                            <path d="M5.2,10.6c0.1,0.1,0.1,0.2,0.2,0.3c-0.4,0.1-0.9,0.1-1.3,0c0-0.1,0.1-0.2,0.2-0.3l0.5-0.5L5.2,10.6z"/>
                            <path d="M3.8,9.1l1,1l-0.5,0.5c-0.1,0.1-0.2,0.2-0.2,0.3c-0.2-0.1-0.3-0.2-0.3-0.3V9.1z"/>
                            <path d="M4.7,12.2c-0.3,0.1-0.7,0-0.9-0.3L0.3,8.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.1,2v1.4c0,0.1,0.1,0.2,0.3,0.3C3.9,11.2,4,11.7,4.2,12C4.4,12.1,4.6,12.2,4.7,12.2z"/>
                        </g>
                    </svg>
                    <span class="sr-only">Read more</span>
                </a>
            </section>
        </div>

        <div class="scene">
            <div class="layer" data-depth="0.20">
                <div class="intro__bg intro__bg--1"></div>
                <div class="intro__bg intro__bg--3"></div>
            </div>
        </div>
    </div>

    <div class="services__container--outer scene">

        <div class="container">
            <section id="services" class="section services__container">
                <?php get_page_custom('Usługi'); ?>

                <div class="gallery__container" data-items-visible="[0,0,0,3,1]" data-item-width="[0,0,0,190,195]">
                    <div class="gallery__inner">
                        <ul class="services gallery map-list">
                            <li class="services__item gallery-item">
                                <a tabindex="0" href="#" role="button" data-container="body"
                                   data-toggle="popover" data-placement="top" data-trigger="hover">
                                    <span class="services__desc"></span>
                                </a>
                            </li>
                        </ul>

                        <div class="hidden list-to-map">
                            <?php get_page_custom('Usługi lista'); ?>
                        </div>

                        <div class="hidden svg-list">
                            <ul>
                                <li class="infr">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="7 2 76 86" xml:space="preserve">
                                        <style type="text/css">
                                            .infr{fill:url(#gradient1);}
                                        </style>
                                        <linearGradient id="gradient1" gradientUnits="userSpaceOnUse" x1="45" y1="87.23" x2="45" y2="2.103">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <path class="infr" d="M78,57.7V32.5c2.9-0.3,5.1-2.8,5.1-5.8c0-3.2-2.6-5.8-5.8-5.8c-1.8,0-3.4,0.8-4.4,2L50.4,10c0.3-0.6,0.4-1.4,0.4-2.1c0-3.2-2.6-5.8-5.8-5.8c-3.2,0-5.8,2.6-5.8,5.8c0,0.7,0.1,1.4,0.4,2.1L17.1,23c-1.1-1.2-2.7-2-4.4-2c-3.2,0-5.8,2.6-5.8,5.8c0,3,2.2,5.4,5.1,5.8v25.2c-2.9,0.3-5.1,2.8-5.1,5.8c0,3.2,2.6,5.8,5.8,5.8c1.9,0,3.5-0.9,4.6-2.2l22.1,12.8c-0.1,0.5-0.2,1-0.2,1.6c0,3.2,2.6,5.8,5.8,5.8c3.2,0,5.8-2.6,5.8-5.8c0-0.6-0.1-1.1-0.2-1.6l22.1-12.8c1.1,1.4,2.7,2.2,4.6,2.2c3.2,0,5.8-2.6,5.8-5.8C83.1,60.5,80.9,58,78,57.7z M14,57.8V32.4c1.3-0.3,2.4-1,3.2-2L39.5,43c-0.2,0.6-0.3,1.3-0.3,2c0,0.6,0.1,1.2,0.3,1.7L16.9,59.5C16.1,58.7,15.1,58.1,14,57.8z M50.4,42.9l22.3-12.6c0.8,1,2,1.8,3.3,2.1v25.4c-1.2,0.3-2.2,0.9-3,1.8L50.5,46.9c0.2-0.6,0.3-1.2,0.3-1.9C50.8,44.3,50.7,43.6,50.4,42.9z M71.9,24.7c-0.2,0.6-0.4,1.3-0.4,2.1c0,0.6,0.1,1.2,0.3,1.8L49.4,41.2c-0.9-1-2.1-1.7-3.4-1.9V13.7c1.4-0.2,2.5-0.9,3.4-1.9L71.9,24.7z M40.6,11.7c0.9,1,2.1,1.7,3.4,1.9v25.6c-1.4,0.2-2.6,1-3.5,2L18.2,28.7c0.2-0.6,0.3-1.3,0.3-2c0-0.7-0.1-1.4-0.4-2.1L40.6,11.7z M18.2,65.3c0.2-0.6,0.3-1.2,0.3-1.8c0-0.8-0.2-1.6-0.5-2.3l22.3-12.6c0.9,1.1,2.2,2,3.6,2.2v24.9c-1.5,0.3-2.9,1.1-3.7,2.3L18.2,65.3zM49.7,78c-0.9-1.2-2.2-2.1-3.7-2.3V50.7c1.4-0.2,2.7-1,3.6-2.1l22.4,12.6c-0.3,0.7-0.4,1.4-0.4,2.2c0,0.6,0.1,1.2,0.3,1.8L49.7,78z"/>
                                    </svg>
                                </li>

                                <li class="research">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="14 40 72 120" xml:space="preserve">
                                    <style type="text/css">
                                        .research{fill:url(#gradient2);stroke:url(#gradient21);stroke-miterlimit:10;}
                                        .research1{fill:url(#gradient22);}
                                        .research2{fill:url(#gradient23);}
                                    </style>
                                        <linearGradient id="gradient2" gradientUnits="userSpaceOnUse" x1="50" y1="1111.4722" x2="50" y2="1025.4722">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient21" gradientUnits="userSpaceOnUse" x1="50" y1="1111.9722" x2="50" y2="1024.9722">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient22" gradientUnits="userSpaceOnUse" x1="43.5938" y1="57.3333" x2="43.5938" y2="40.3333">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient23" gradientUnits="userSpaceOnUse" x1="61.078" y1="65.0818" x2="61.078" y2="53.3333">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <g transform="translate(0,-952.36218)">
                                            <path class="research" d="M35,1025.5c-1.6,0-3,1.4-3,3v6c0,1.6,1.4,3,3,3h2v16.8c-6.5,14-13.8,29.5-19.9,42.7l0,0.1c-1.3,2.2-2.1,4.4-2.1,6.4c0,2.3,0.7,4.4,2.3,5.8c1.6,1.4,3.8,2.2,6.7,2.2h52c2.9,0,5.2-0.8,6.7-2.2c1.6-1.4,2.3-3.5,2.3-5.8c0-2.1-0.8-4.2-2.1-6.4l0-0.1L63,1054.3v-16.8h2c1.6,0,3-1.4,3-3v-6c0-1.6-1.4-3-3-3H35z M35,1027.5h30c0.6,0,1,0.4,1,1v6c0,0.6-0.4,1-1,1h-3h-1v1c0,6.1,0,12.2,0,18.2c4,8.5,7.9,17,11.8,25.4c-5.1,4.8-8,6.3-12.8,6.3c-3.7,0-7.2-2.3-10.8-4.8c-3.6-2.5-7.5-5.2-12.2-5.2c-3.5,0-6.7,1-9.7,3.3c3.9-8.3,7.7-16.7,11.7-25c0-6.1,0-12.2,0-18.2v-1h-1h-3c-0.6,0-1-0.4-1-1v-6C34,1027.9,34.4,1027.5,35,1027.5z M37,1078.5c3.9,0,7.4,2.3,11,4.8s7.4,5.2,12,5.2c5.1,0,8.7-1.8,13.8-6.4l7.3,15.8l0,0v0c1.2,2.1,1.9,4,1.9,5.5c0,1.9-0.6,3.3-1.7,4.3s-2.8,1.7-5.3,1.7H24c-2.5,0-4.3-0.7-5.3-1.7c-1.1-1-1.7-2.4-1.7-4.3c0-1.5,0.7-3.4,1.9-5.5v0l0,0l6-13C28.8,1080.2,32.4,1078.5,37,1078.5z"/>
                                        </g>
                                        <circle class="research1" cx="43.6" cy="48.8" r="8.5"/>
                                        <circle class="research2" cx="61.1" cy="59.2" r="5.9"/>
                                        <circle class="research3" cx="35.1" cy="139" r="4.8"/>
                                    </svg>
                                </li>

                                <li class="develop">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="2 7 32 22" xml:space="preserve">
                                    <style type="text/css">
                                        .develop{fill:url(#gradient3);}
                                    </style>
                                        <linearGradient id="gradient3" gradientUnits="userSpaceOnUse" x1="18" y1="24.5938" x2="18" y2="3.4062">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <g transform="translate(0,4)">
                                            <path class="develop" d="M31.6,3.4c-1.4,0-2.5,1.1-2.5,2.5c0,0.6,0.2,1.1,0.5,1.5l-8.5,10.4c-0.3-0.1-0.6-0.2-0.9-0.2c-0.3,0-0.5,0.1-0.8,0.2L15.6,13c0.1-0.3,0.2-0.7,0.2-1.1c0-1.4-1.2-2.6-2.6-2.6s-2.6,1.2-2.6,2.6c0,0.3,0,0.6,0.2,0.9l-5.4,7c-0.3-0.1-0.6-0.2-1-0.2c-1.4,0-2.5,1.1-2.5,2.5s1.1,2.5,2.5,2.5s2.5-1.1,2.5-2.5c0-0.7-0.3-1.3-0.7-1.7l5.2-6.6c0.5,0.5,1.1,0.8,1.8,0.8c0.7,0,1.3-0.3,1.8-0.7l3.6,4.4c-0.5,0.5-0.9,1.1-0.9,1.9c0,1.4,1.1,2.5,2.5,2.5s2.5-1.1,2.5-2.5c0-0.7-0.3-1.3-0.7-1.8l8.4-10.3c0.4,0.2,0.8,0.3,1.2,0.3c1.4,0,2.5-1.1,2.5-2.5S33,3.4,31.6,3.4z"/>
                                        </g>
                                    </svg>
                                </li>

                                <li class="demo">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="0 0 60 58.5" xml:space="preserve">
                                    <style type="text/css">
                                        .demo{fill:url(#gradient4);}
                                    </style>
                                        <linearGradient id="gradient4" gradientUnits="userSpaceOnUse" x1="30" y1="58.4896" x2="30" y2="-9.502411e-03">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <path class="demo" d="M1,3h58c0.6,0,1,0.4,1,1v1c0,0.6-0.4,1-1,1H1C0.4,6,0,5.5,0,5V4C0,3.4,0.4,3,1,3z M1,42.9h58c0.6,0,1,0.4,1,1v1c0,0.6-0.4,1-1,1H1c-0.6,0-1-0.4-1-1v-1C0,43.4,0.4,42.9,1,42.9z M5,3.5c0.6,0,1,1.4,1,2v37.9c0,0.5-0.4,2-1,2s-1-1.4-1-2V5.5C4,4.9,4.4,3.5,5,3.5z M54.5,3.5c0.6,0,1,1.4,1,2v37.9c0,0.5-0.4,2-1,2s-1-1.4-1-2V5.5C53.5,4.9,54,3.5,54.5,3.5zM19,12.5c1.9,0,3.5,1.6,3.5,3.5c0,1.9-1.6,3.5-3.5,3.5c-1.9,0-3.5-1.6-3.5-3.5C15.5,14,17.1,12.5,19,12.5C19,12.5,19,12.5,19,12.5zM19,21.5c1.9,0,3.5,1.6,3.5,3.5c0,1.9-1.6,3.5-3.5,3.5c-1.9,0-3.5-1.6-3.5-3.5C15.5,23,17.1,21.5,19,21.5C19,21.5,19,21.5,19,21.5L19,21.5z M19,29.9c1.9,0,3.5,1.6,3.5,3.5c0,1.9-1.6,3.5-3.5,3.5c-1.9,0-3.5-1.6-3.5-3.5C15.5,31.5,17.1,29.9,19,29.9C19,29.9,19,29.9,19,29.9L19,29.9z M27.8,15h16.5c0.4,0,0.8,0.3,0.8,0.8v0.5c0,0.4-0.3,0.8-0.8,0.8H27.8c-0.4,0-0.8-0.3-0.8-0.8v-0.5C27,15.3,27.3,15,27.8,15z M27.8,23.5h16.5c0.4,0,0.8,0.3,0.8,0.8v0.5c0,0.4-0.3,0.8-0.8,0.8H27.8c-0.4,0-0.8-0.3-0.8-0.8v-0.5C27,23.8,27.3,23.5,27.8,23.5C27.7,23.5,27.7,23.5,27.8,23.5L27.8,23.5z M27.8,32.4h16.5c0.4,0,0.8,0.3,0.8,0.8v0.5c0,0.4-0.3,0.8-0.8,0.8H27.8c-0.4,0-0.8-0.3-0.8-0.8v-0.5C27,32.8,27.3,32.4,27.8,32.4L27.8,32.4z M20.8,43.3c0.6,0.2,1,1.9,0.8,2.6l-4.2,11.8c-0.2,0.6-1,1-1.6,0.7c-0.6-0.2-0.9-0.9-0.7-1.6l4.2-11.8C19.4,44.4,20.1,43.1,20.8,43.3L20.8,43.3z M18,50.4h23c0.6,0,1,0.4,1,1v0c0,0.6-0.4,1-1,1H18c-0.6,0-1-0.4-1-1v0C17,50.8,17.4,50.4,18,50.4z M27,3.5C27,1.6,28.6,0,30.5,0S34,1.6,34,3.5M38.3,43.3c0.7-0.2,1.4,1.1,1.6,1.8l4.2,11.8c0.2,0.6-0.1,1.3-0.7,1.6c-0.6,0.2-1.4-0.1-1.6-0.7l-4.2-11.8C37.3,45.3,37.6,43.6,38.3,43.3L38.3,43.3z"/>
                                    </svg>
                                </li>

                                <li class="implement">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="0 0 58.5 71.5" xml:space="preserve">
                                    <style type="text/css">
                                        .implement{fill:url(#gradient5);}
                                    </style>
                                        <linearGradient id="gradient5" gradientUnits="userSpaceOnUse" x1="29.2525" y1="71.5005" x2="29.2525" y2="5.500000e-03">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <path class="implement" d="M30.6,61.6c2.7,0,4.9,2.2,4.9,4.9s-2.2,4.9-4.9,4.9s-4.9-2.2-4.9-4.9c0,0,0,0,0,0C25.7,63.8,27.9,61.6,30.6,61.6C30.6,61.6,30.6,61.6,30.6,61.6z M0.9,66.5h56.7c0.5,0,0.9,0.4,0.9,0.9l0,0l0,0c0,0.5-0.4,0.9-0.9,0.9h0H0.9c-0.5,0-0.9-0.4-0.9-0.9c0,0,0,0,0,0l0,0C0,66.9,0.4,66.5,0.9,66.5C0.9,66.5,0.9,66.5,0.9,66.5zM1.5,13.8L44,56.5l13.4-0.1l0.1-13.5L15,0.2L1.5,13.8z M55.3,54.3l-9.9,0.1l9.9-10L55.3,54.3z M54.3,42.6L43.6,53.2L6.5,15.9L17.1,5.2L54.3,42.6z M1.5,13.8L1.5,13.8l42.4,42.8l13.5-0.1l0.1-13.6L15,0L1.4,13.8H1.5L1.5,13.8L1.5,13.8L15,0.3L57.4,43l-0.1,13.3L44,56.4L1.5,13.8L1.5,13.8L1.5,13.8L1.5,13.8z M55.3,54.3v-0.1l-9.6,0.1l9.6-9.6l-0.1,9.7L55.3,54.3l0-0.1L55.3,54.3l0.1,0l0.1-10.2L45.2,54.5l10.2-0.1L55.3,54.3L55.3,54.3z M54.3,42.6L54.3,42.6L43.6,53.1l-37-37.1L17.1,5.4L54.3,42.6L54.3,42.6L54.3,42.6L54.3,42.6L54.3,42.6L17.1,5.1L6.3,15.9l37.3,37.4l10.8-10.8H54.3L54.3,42.6z"/>
                                    </svg>
                                </li>

                                <li class="comm">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="20 14 61 72" xml:space="preserve">
                                    <style type="text/css">
                                        .comm{fill:url(#gradient6);stroke:url(#gradient61);stroke-miterlimit:10;}
                                    </style>
                                        <g>
                                            <linearGradient id="gradient6" gradientUnits="userSpaceOnUse" x1="49.9997" y1="84.808" x2="49.9997" y2="15.1928">
                                                <stop offset="0.7" style="stop-color:#D72D2F"/>
                                                <stop offset="1" style="stop-color:#8D263C"/>
                                            </linearGradient>
                                            <linearGradient id="gradient61" gradientUnits="userSpaceOnUse" x1="49.9997" y1="85.308" x2="49.9997" y2="14.6928">
                                                <stop offset="0.7" style="stop-color:#D72D2F"/>
                                                <stop offset="1" style="stop-color:#8D263C"/>
                                            </linearGradient>
                                            <path class="comm" d="M35.4,15.2c-0.5,0.1-0.8,0.8-0.6,1.2l2.9,5.1c0.2,0.4,0.7,0.6,1.1,0.3c0.4-0.2,0.5-0.8,0.3-1.2l-3-5.1C36.1,15.3,35.7,15.2,35.4,15.2L35.4,15.2z M64.3,15.2c-0.3,0.1-0.5,0.2-0.6,0.4l-2.9,5.1c-0.3,0.4-0.1,0.9,0.3,1.2c0.4,0.2,0.9,0.1,1.1-0.3l3-5.1C65.5,15.9,65,15.1,64.3,15.2C64.4,15.2,64.3,15.2,64.3,15.2L64.3,15.2z M50,21.3c-10.5,0-19.1,8.7-19.1,19.3c0,6.3,1.8,9.4,3.5,12.2c1.7,2.8,3.2,5.2,3.2,10.7c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.1c0,0.1,0,0.1,0,0.1l3,15.4c0.3,1.3,1.5,2.1,2.9,2.1h0.3v0.4c0,1.6,1.1,3,2.6,3h6.9c1.5,0,2.7-1.4,2.7-3v-0.4h0.2c1.4,0,2.7-0.8,3-2.1l3-15.4l0-0.1l0,0c0-0.1,0-0.2,0-0.3c0,0,0-0.1,0-0.1c0.1-5.3,1.6-7.7,3.3-10.5c1.7-2.8,3.5-6,3.5-12.2C69,30,60.5,21.3,50,21.3L50,21.3zM50,22.9c9.6,0,17.5,7.9,17.5,17.7c0,6-1.6,8.6-3.3,11.4c-1.6,2.6-3.3,5.5-3.4,10.7h-7.6h-6.4h-7.6c-0.2-5.2-1.8-8.1-3.4-10.7c-1.7-2.8-3.3-5.4-3.3-11.4C32.5,30.9,40.4,22.9,50,22.9L50,22.9z M24.8,25.8c-0.7,0.1-0.9,1.2-0.3,1.5l5.1,3c0.4,0.2,0.9,0.1,1.1-0.3c0.2-0.4,0.1-0.9-0.3-1.1l-5.1-2.9C25.2,25.8,25,25.8,24.8,25.8C24.9,25.8,24.8,25.8,24.8,25.8L24.8,25.8zM75,25.8c-0.1,0-0.3,0.1-0.4,0.1l-5.1,2.9c-0.4,0.2-0.5,0.7-0.3,1.1c0.2,0.4,0.7,0.5,1.1,0.3l5.1-3c0.3-0.2,0.5-0.6,0.4-0.9C75.7,26,75.4,25.8,75,25.8L75,25.8z M21,40.3c-0.4,0-0.8,0.4-0.8,0.8c0,0.4,0.4,0.8,0.8,0.7h5.8c0.4,0,0.8-0.4,0.8-0.8c0-0.4-0.4-0.8-0.8-0.8H21C21,40.3,21,40.3,21,40.3L21,40.3z M73,40.3c-0.4,0-0.8,0.4-0.8,0.8c0,0.4,0.4,0.8,0.8,0.7H79c0.4,0,0.8-0.4,0.8-0.8c0-0.4-0.4-0.8-0.8-0.8H73C73.1,40.3,73,40.3,73,40.3L73,40.3z M39.5,64.3h21.1l-2.8,14.5c-0.1,0.3-0.7,0.9-1.4,0.9h-0.9h-0.1h-8.5h-2.3h-0.9c-0.7,0-1.3-0.6-1.3-0.9l-0.7-3.7h10.3c0.4,0,0.8-0.4,0.8-0.8c0-0.4-0.4-0.8-0.8-0.8H41.2l-0.8-4.4H54c0.4,0,0.8-0.4,0.8-0.8s-0.4-0.8-0.8-0.8H40.1L39.5,64.3L39.5,64.3z M45.4,81.3h1.4h7.7v0.4c0,0.9-0.6,1.5-1.1,1.5h-6.9c-0.5,0-1.1-0.6-1.1-1.5L45.4,81.3L45.4,81.3z"/>
                                        </g>
                                    </svg>
                                </li>

                                <li class="expansion">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                         viewBox="0 0 48 48" xml:space="preserve">
                                    <style type="text/css">
                                        .expansion{fill:none;stroke:url(#gradient70);stroke-width:2;stroke-miterlimit:10;}
                                        .expansion1{fill:none;stroke:url(#gradient71);stroke-width:2;stroke-miterlimit:10;}
                                        .expansion2{fill:none;stroke:url(#gradient72);stroke-width:2;stroke-miterlimit:10;}
                                        .expansion3{fill:none;stroke:url(#gradient73);stroke-width:2;stroke-miterlimit:10;}
                                        .expansion4{fill:none;stroke:url(#gradient74);stroke-width:2;stroke-miterlimit:10;}
                                        .expansion5{fill:none;stroke:url(#gradient75);stroke-width:2;stroke-miterlimit:10;}
                                    </style>
                                        <linearGradient id="gradient70" gradientUnits="userSpaceOnUse" x1="24" y1="47.1" x2="24" y2="0.9">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient71" gradientUnits="userSpaceOnUse" x1="24" y1="47.1" x2="24" y2="0.9">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient72" gradientUnits="userSpaceOnUse" x1="24" y1="47" x2="24" y2="1">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient73" gradientUnits="userSpaceOnUse" x1="24" y1="36.3" x2="24" y2="34.3">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient74" gradientUnits="userSpaceOnUse" x1="24" y1="25" x2="24" y2="23">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <linearGradient id="gradient75" gradientUnits="userSpaceOnUse" x1="24" y1="13.7" x2="24" y2="11.7">
                                            <stop offset="0.7" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#8D263C"/>
                                        </linearGradient>
                                        <g>
                                            <circle class="expansion" cx="24" cy="24" r="22.1"/>
                                            <ellipse class="expansion1" cx="24" cy="24" rx="12.3" ry="22.1"/>
                                            <line class="expansion2" x1="24" y1="1" x2="24" y2="47"/>
                                            <line class="expansion3" x1="43.4" y1="35.3" x2="4.6" y2="35.3"/>
                                            <line class="expansion4" x1="47" y1="24" x2="1" y2="24"/>
                                            <line class="expansion5" x1="43.4" y1="12.7" x2="4.6" y2="12.7"/>
                                        </g>
                                    </svg>
                                </li>
                            </ul>
                        </div>

                        <div class="coords-axis gallery__additional">
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point coords-axis__point--marker"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point coords-axis__point--marker"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point coords-axis__point--marker"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point coords-axis__point--marker"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point coords-axis__point--marker"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point coords-axis__point--marker"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__point"></div>
                            <div class="coords-axis__arrow"></div>
                        </div>
                    </div>

                    <div class="services-nav gallery-nav">
                        <a href="#" class="gallery-nav__prev">
                            <svg class="gallery-nav__arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 viewBox="0 2 13.5 38" style="enable-background:new 0 0 14 42.1;" xml:space="preserve">
                                <path d="M3.5,21l9.7-16.8c0,0,0,0,0,0c0.4-0.6,0.1-1.5-0.5-1.8c0,0,0,0,0,0C12,2,11.2,2.3,10.8,2.9L1,19.9c-0.2,0.3-0.3,0.5-0.3,1.1c0,0.4,0.1,0.7,0.3,1.1l9.8,17c0.4,0.6,1.2,0.9,1.8,0.5c0.6-0.4,0.9-1.2,0.5-1.8L3.5,21z"/>
                            </svg>

                            <span class="sr-only">Previous</span>
                        </a>

                        <a href="#" class="gallery-nav__next">
                            <svg class="gallery-nav__arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 viewBox="0 2 13.5 38" style="enable-background:new 0 0 14 42.1;" xml:space="preserve">
                                <path d="M3.5,21l9.7-16.8c0,0,0,0,0,0c0.4-0.6,0.1-1.5-0.5-1.8c0,0,0,0,0,0C12,2,11.2,2.3,10.8,2.9L1,19.9c-0.2,0.3-0.3,0.5-0.3,1.1c0,0.4,0.1,0.7,0.3,1.1l9.8,17c0.4,0.6,1.2,0.9,1.8,0.5c0.6-0.4,0.9-1.2,0.5-1.8L3.5,21z"/>
                            </svg>

                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </section>
        </div>

        <div class="layer" data-depth="0.05">
            <div class="services__bg"></div>
        </div>
    </div>

    <div class="process__container">
        <div class="container">
            <section id="process" class="section process">
                <?php get_page_custom('Proces'); ?>

                <div class="steps__container">
                    <ul class="steps map-list">
                        <li class="steps__item">
                            <span class="steps__desc"></span>

                            <a tabindex="0" href="#" role="button" class="btn steps__button" data-container="body"
                               data-toggle="popover" data-placement="top" data-trigger="hover">
                                <svg version="1.1" class="inactive" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     viewBox="1 0 39 40" style="enable-background:new 0 0 40.6 40.2;" xml:space="preserve">
                                    <style type="text/css">
                                        .circle{fill:#FAFAFA;}
                                        .circle2{fill:url(#gradient-step);}
                                    </style>

                                    <g>
                                        <circle class="circle" cx="20.4" cy="20.1" r="16.4"/>

                                        <linearGradient id="gradient-step" gradientUnits="userSpaceOnUse" x1="965.375" y1="1131.5695" x2="965.375" y2="1062.6805" gradientTransform="matrix(0.5 0 0 0.5 -462.3125 -528.4375)">
                                            <stop offset="0" style="stop-color:#D72D2F"/>
                                            <stop offset="0.704" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#251244"/>
                                        </linearGradient>

                                        <path class="circle2" d="M20.4,39.5C9.7,39.5,1,30.8,1,20.1S9.7,0.7,20.4,0.7s19.4,8.7,19.4,19.4S31.1,39.5,20.4,39.5z M20.4,6.7C13,6.7,7,12.7,7,20.1s6,13.4,13.4,13.4s13.4-6,13.4-13.4S27.8,6.7,20.4,6.7z"/>
                                    </g>
                                </svg>

                                <svg version="1.1" class="active" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                                    <style type="text/css">
                                        .circle3{fill:url(#gradient-step2);}
                                    </style>

                                    <linearGradient id="gradient-step2" gradientUnits="userSpaceOnUse" x1="12" y1="24" x2="12" y2="9.094947e-13">
                                        <stop offset="0.7" style="stop-color:#D72D2F"/>
                                        <stop offset="1" style="stop-color:#8D263C"/>
                                    </linearGradient>

                                    <circle class="circle3" cx="12" cy="12" r="12"/>
                                </svg>

                                <span class="sr-only">Read more</span>
                            </a>
                        </li>

                        <li class="steps__item steps__item--first">
                            <span class="steps__desc"></span>

                            <a tabindex="0" href="#" role="button" class="btn steps__button" data-container="body"
                               data-toggle="popover" data-placement="top" data-trigger="hover">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     viewBox="0 0 41.4 41.1" style="enable-background:new 0 0 41.4 41.1;" xml:space="preserve">
                                    <style type="text/css">
                                        .circle4{fill:#FFFFFF;}
                                        .path{fill:url(#gradient-circle4);}
                                        .circle5{fill:#D72D2F;}
                                    </style>

                                    <g>
                                        <circle class="circle4" cx="20.5" cy="20.5" r="16.8"/>

                                        <linearGradient id="gradient-circle4" gradientUnits="userSpaceOnUse" x1="499.5" y1="-292.8478" x2="499.5" y2="-220.1522" gradientTransform="matrix(0.5 0 0 -0.5 -229.25 -107.75)">
                                            <stop offset="0" style="stop-color:#D72D2F"/>
                                            <stop offset="0.704" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#251244"/>
                                        </linearGradient>

                                        <path class="path" d="M20.5,40.3c-10.9,0-19.8-8.9-19.8-19.8S9.6,0.7,20.5,0.7s19.8,8.9,19.8,19.8S31.4,40.3,20.5,40.3z M20.5,6.7c-7.6,0-13.8,6.2-13.8,13.8s6.2,13.8,13.8,13.8s13.8-6.2,13.8-13.8S28.1,6.7,20.5,6.7z"/>
                                    </g>

                                    <circle class="circle5" cx="20.5" cy="20.5" r="6.8"/>
                                </svg>

                                <span class="sr-only">Read more</span>
                            </a>
                        </li>

                        <li class="steps__item steps__item--right steps__item--final">
                            <span class="steps__desc"></span>

                            <a tabindex="0" href="#" role="button" class="btn steps__button" data-container="body"
                               data-toggle="popover" data-placement="top" data-trigger="hover">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     viewBox="0 0 41.4 41.1" style="enable-background:new 0 0 41.4 41.1;" xml:space="preserve">
                                    <style type="text/css">
                                        .circle4{fill:#FFFFFF;}
                                        .path{fill:url(#gradient-circle4);}
                                        .circle5{fill:#D72D2F;}
                                    </style>

                                    <g>
                                        <circle class="circle4" cx="20.5" cy="20.5" r="16.8"/>

                                        <linearGradient id="gradient-circle4" gradientUnits="userSpaceOnUse" x1="499.5" y1="-292.8478" x2="499.5" y2="-220.1522" gradientTransform="matrix(0.5 0 0 -0.5 -229.25 -107.75)">
                                            <stop offset="0" style="stop-color:#D72D2F"/>
                                            <stop offset="0.704" style="stop-color:#D72D2F"/>
                                            <stop offset="1" style="stop-color:#251244"/>
                                        </linearGradient>

                                        <path class="path" d="M20.5,40.3c-10.9,0-19.8-8.9-19.8-19.8S9.6,0.7,20.5,0.7s19.8,8.9,19.8,19.8S31.4,40.3,20.5,40.3z M20.5,6.7c-7.6,0-13.8,6.2-13.8,13.8s6.2,13.8,13.8,13.8s13.8-6.2,13.8-13.8S28.1,6.7,20.5,6.7z"/>
                                    </g>

                                    <circle class="circle5" cx="20.5" cy="20.5" r="6.8"/>
                                </svg>

                                <span class="sr-only">Read more</span>
                            </a>
                        </li>
                    </ul>

                    <div class="hidden list-to-map">
                        <?php get_page_custom('Proces lista'); ?>
                    </div>

                    <div class="steps-lines">
                        <div class="steps-lines__item steps-lines__item--1"></div>
                        <div class="steps-lines__item steps-lines__item--2"></div>
                        <div class="steps-lines__item steps-lines__item--3"></div>
                        <div class="steps-lines__item steps-lines__item--4"></div>
                        <div class="steps-lines__item steps-lines__item--5"></div>
                        <div class="steps-lines__item steps-lines__item--6"></div>
                        <div class="steps-lines__item steps-lines__item--7"></div>

                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--1 steps-lines__item--active--1"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--2 steps-lines__item--active--2"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--3 steps-lines__item--active--3"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--4 steps-lines__item--active--4"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--5 steps-lines__item--active--5"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--6 steps-lines__item--active--6"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--7 steps-lines__item--active--7"></div>

                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--8"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--9"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--10"></div>

                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--11"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--12"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--13"></div>

                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--14"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--15"></div>
                        <div class="steps-lines__item steps-lines__item--active steps-lines__item--16"></div>

                        <div class="steps-lines__item steps-lines__item--17"></div>
                        <div class="steps-lines__item steps-lines__item--18"></div>
                        <div class="steps-lines__item steps-lines__item--19"></div>
                        <div class="steps-lines__item steps-lines__item--20"></div>
                        <div class="steps-lines__item steps-lines__item--21"></div>
                        <div class="steps-lines__item steps-lines__item--22"></div>
                        <div class="steps-lines__item steps-lines__item--23"></div>
                        <div class="steps-lines__item steps-lines__item--24"></div>
                        <div class="steps-lines__item steps-lines__item--25"></div>
                        <div class="steps-lines__item steps-lines__item--26"></div>
                        <div class="steps-lines__item steps-lines__item--27"></div>
                        <div class="steps-lines__item steps-lines__item--28"></div>
                        <div class="steps-lines__item steps-lines__item--29"></div>
                        <div class="steps-lines__item steps-lines__item--30"></div>
                        <div class="steps-lines__item steps-lines__item--31"></div>
                        <div class="steps-lines__item steps-lines__item--32"></div>
                        <div class="steps-lines__item steps-lines__item--33"></div>
                        <div class="steps-lines__item steps-lines__item--34"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="scene">
        <div class="layer" data-depth="0.08">
            <div class="process__bg"></div>
        </div>
    </div>

    <div class="why-us__container scene">
        <div class="container">
            <section id="why-us" class="section why-us">
                <?php get_page_custom('Dlaczego my'); ?>
            </section>
        </div>

        <div class="layer" data-depth="0.03">
            <div class="why-us__bg"></div>
        </div>
    </div>

    <div class="container" id="clients-gallery">
        <section class="gallery__container" data-items-visible="[5,4,3,3,1]" data-item-width="[234,234,220,156,180]" data-autoplay="true" data-loop="true">
            <div class="gallery__inner">
                <?php echo do_shortcode(get_post_field('post_content', url_to_postid('galeria-klientow-' . pll_current_language()))); ?>
            </div>

            <div class="gallery-nav">
                <a href="#" class="gallery-nav__prev">
                    <svg class="gallery-nav__arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         viewBox="0 2 13.5 38" style="enable-background:new 0 0 14 42.1;" xml:space="preserve">
                        <path d="M3.5,21l9.7-16.8c0,0,0,0,0,0c0.4-0.6,0.1-1.5-0.5-1.8c0,0,0,0,0,0C12,2,11.2,2.3,10.8,2.9L1,19.9c-0.2,0.3-0.3,0.5-0.3,1.1c0,0.4,0.1,0.7,0.3,1.1l9.8,17c0.4,0.6,1.2,0.9,1.8,0.5c0.6-0.4,0.9-1.2,0.5-1.8L3.5,21z"/>
                    </svg>

                    <span class="sr-only">Previous</span>
                </a>

                <a href="#" class="gallery-nav__next">
                    <svg class="gallery-nav__arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         viewBox="0 2 13.5 38" style="enable-background:new 0 0 14 42.1;" xml:space="preserve">
                        <path d="M3.5,21l9.7-16.8c0,0,0,0,0,0c0.4-0.6,0.1-1.5-0.5-1.8c0,0,0,0,0,0C12,2,11.2,2.3,10.8,2.9L1,19.9c-0.2,0.3-0.3,0.5-0.3,1.1c0,0.4,0.1,0.7,0.3,1.1l9.8,17c0.4,0.6,1.2,0.9,1.8,0.5c0.6-0.4,0.9-1.2,0.5-1.8L3.5,21z"/>
                    </svg>

                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>
    </div>

    <div class="know-how__container scene">
        <div class="container">
            <section id="know-how" class="section know-how">
                <?php get_page_custom('Know how'); ?>

                <span class="section__separator"></span>
            </section>
        </div>

        <div class="layer" data-depth="0.05">
            <div class="know-how__bg"></div>
        </div>
    </div>

    <div class="staff__container scene">
        <div class="container">
            <section id="staff" class="gallery__container gallery__container--wide staff" data-items-visible="[5,4,3,2,1]" data-item-width="[234,234,220,234,180]">
                <div class="gallery__inner gallery__inner--wide">
                    <?php echo do_shortcode(get_post_field('post_content', url_to_postid('galeria-pracownikow-' . pll_current_language()))); ?>

                    <figure class="hidden gallery-item gallery-item--link">
                        <?php $url = get_category(
                            pll_get_term(
                                get_category_by_slug('dolacz-do-nas') -> term_id
                            )
                        ) -> slug; ?>
                        <a href="<?php print $url; ?>">
                            <span><?php pll_e('Dołącz'); ?></span>
                            <span class="gallery-item__plus"></span>
                        </a>
                    </figure>
                </div>

                <div class="gallery-nav">
                    <a href="#" class="gallery-nav__prev">
                        <svg class="gallery-nav__arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                             viewBox="0 2 13.5 38" style="enable-background:new 0 0 14 42.1;" xml:space="preserve">
                            <path d="M3.5,21l9.7-16.8c0,0,0,0,0,0c0.4-0.6,0.1-1.5-0.5-1.8c0,0,0,0,0,0C12,2,11.2,2.3,10.8,2.9L1,19.9c-0.2,0.3-0.3,0.5-0.3,1.1c0,0.4,0.1,0.7,0.3,1.1l9.8,17c0.4,0.6,1.2,0.9,1.8,0.5c0.6-0.4,0.9-1.2,0.5-1.8L3.5,21z"/>
                        </svg>

                        <span class="sr-only">Previous</span>
                    </a>

                    <a href="#" class="gallery-nav__next">
                        <svg class="gallery-nav__arrow" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                             viewBox="0 2 13.5 38" style="enable-background:new 0 0 14 42.1;" xml:space="preserve">
                            <path d="M3.5,21l9.7-16.8c0,0,0,0,0,0c0.4-0.6,0.1-1.5-0.5-1.8c0,0,0,0,0,0C12,2,11.2,2.3,10.8,2.9L1,19.9c-0.2,0.3-0.3,0.5-0.3,1.1c0,0.4,0.1,0.7,0.3,1.1l9.8,17c0.4,0.6,1.2,0.9,1.8,0.5c0.6-0.4,0.9-1.2,0.5-1.8L3.5,21z"/>
                        </svg>

                        <span class="sr-only">Next</span>
                    </a>

                    <ul class="gallery-nav__bullets">
                        <li class="gallery-nav__bullet gallery-nav__bullet--active">
                            <a href="#" class="gallery-nav__link">
                                <span class="sr-only">1</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
