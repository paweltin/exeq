<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<li class="job-list__item">
    <a href="#" class="job-list__title" data-toggle="modal" data-target="#job-<?php the_ID(); ?>">
        <?php the_title(); ?>

        <svg version="1.1" class="inactive" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
             viewBox="1 0 39 40" style="enable-background:new 0 0 40.6 40.2;" xml:space="preserve">
            <style type="text/css">
                .circle{fill:#FAFAFA;}
                .circle2{fill:url(#gradient-step);}
            </style>

            <g>
                <circle class="circle" cx="20.4" cy="20.1" r="16.4"/>

                <path class="circle2" d="M20.4,39.5C9.7,39.5,1,30.8,1,20.1S9.7,0.7,20.4,0.7s19.4,8.7,19.4,19.4S31.1,39.5,20.4,39.5z M20.4,6.7C13,6.7,7,12.7,7,20.1s6,13.4,13.4,13.4s13.4-6,13.4-13.4S27.8,6.7,20.4,6.7z"/>
            </g>
        </svg>
    </a>

    <div class="modal fade" id="job-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="job description <?php the_ID(); ?>">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <?php the_title( '<h2>', '</h2>' ); ?>
                </div>

                <div class="modal-body">
                    <?php the_content(); ?>
                </div>

                <div class="modal-footer">
                    <span>
                        Apply now:
                        <a href="mailto:jobs@exeq.eu">jobs@exeq.eu</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</li>
