<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage EXEQ
 */

    $if =  !is_home();
    session_start();
    $visited = $_SESSION['visited'];

    if (!isset($_SESSION['started'])) {
        $_SESSION['started'] = $_SERVER['REQUEST_TIME'];
    }

    if (!$_SESSION['visited']) {
        $_SESSION['visited'] = true;
    }

    CONST _24h = 86400;

    if ($_SERVER['REQUEST_TIME'] - $_SESSION['started'] >= _24h) {
        session_unset();
    }

    $exeq_prefix = $if ? '/' . pll_current_language() . '/' : '';
    get_header(); ?>
    <a href="<?php print $exeq_prefix; ?>" class="logo__container">
        <img src="<?php echo get_template_directory_uri(); ?>/img/dot.jpg" data-src="<?php echo get_template_directory_uri(); ?>/img/<?php print ($visited ? 'logo.png' : 'logo.gif'); ?>" alt="EXEQ" class="logo logo--narrow<?php $if || $visited ? print ' active2': null;?>" />

        <div class="dot__container">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
        </div>
    </a>

    <?php if ( $if ) :
        get_template_part( 'template-parts/content-intro', get_post_format() );
        while ( have_posts() ) : the_post();
            get_template_part( 'template-parts/content', get_post_format() );
        endwhile;
        get_template_part( 'template-parts/content-outro', get_post_format() );
    else:
        get_template_part( 'template-parts/content-index' );
    endif; ?>

    <div class="newsletter__container scene">
        <section class="section">
        <?php if( function_exists( 'mc4wp_show_form' ) ) {
            get_page_custom('Newsletter');
            mc4wp_show_form();
        } ?>
        </section>

        <div class="layer" data-depth="0.10">
            <div class="newsletter__bg"></div>
        </div>
    </div>

    <?php if(!$if) { ?>
    <a href="#top" class="back-to-top">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 26.2 35.8" style="enable-background:new 0 0 26.2 35.8;" xml:space="preserve">
            <path d="M13,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c-0.6,0.1-1.2,0.3-1.6,0.7L0.9,11.2
                c-1.2,1.2-1.2,2.9,0,4.1c1.2,1.2,2.9,1.2,4.1,0l5.2-5.4v23.1c0,1.7,1.2,2.9,2.9,2.9s2.9-1.2,2.9-2.9V10.2l5.3,5
                c1.2,1.2,2.9,1.2,4.1,0c1.2-1.2,1.2-2.9,0-4.1L15.1,0.9c-0.4-0.4-0.9-0.7-1.4-0.8c0,0,0,0,0,0c0,0,0,0,0,0C13.5,0,13.3,0,13,0z"/>
        </svg>
        Back to top
    </a>
    <?php } get_footer(); ?>
</div>
