<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

global $if;
global $visited;
global $exeq_prefix;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300|Open+Sans:300,400,400i,600,700&amp;subset=latin-ext" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".scrollspy" data-offset="15" id="top">
<header class="header<?php $if || $visited ? print ' active2': null;?>">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>

                    <span class="pull-left">
                        <span class="icon-bar icon-bar--first"></span>
                        <span class="icon-bar icon-bar--left"></span>
                        <span class="icon-bar icon-bar--right"></span>
                        <span class="icon-bar icon-bar--last"></span>
                    </span>

                    <span class="pull-right">
                        <span class="navbar-toggle__text">Menu</span>
                    </span>
                </button>

                <?php if ($if) { ?>
                <a href="/" type="button" class="navbar-home">
                    <span class="pull-left">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 52 45" style="enable-background:new 0 0 52 45;" xml:space="preserve">
                        <g>
                            <path d="M44.7,45H7.4c-0.9,0-1.6-0.7-1.6-1.5V25.3c0-0.9,0.7-1.5,1.6-1.5S9,24.5,9,25.3v16.6h34.2V25.3
                                c0-0.9,0.7-1.5,1.6-1.5c0.9,0,1.6,0.7,1.6,1.5v18.1C46.3,44.3,45.6,45,44.7,45z"/>
                            <path d="M1.6,24.3c-0.4,0-0.9-0.2-1.2-0.5c-0.6-0.6-0.5-1.6,0.2-2.2L25.2,0.4c0.6-0.5,1.4-0.5,2,0l7.4,6.2V5.6
                                c0-0.9,0.7-1.5,1.6-1.5h5.4c0.9,0,1.6,0.7,1.6,1.5v8.6l8.3,7.4c0.6,0.6,0.7,1.5,0.1,2.2c-0.6,0.6-1.6,0.7-2.2,0.1L40.6,16
                                c-0.3-0.3-0.5-0.7-0.5-1.2V7.2h-2.3v2.7c0,0.6-0.3,1.1-0.9,1.4c-0.5,0.3-1.2,0.2-1.7-0.2l-9-7.5L2.6,23.9
                                C2.3,24.2,1.9,24.3,1.6,24.3z"/>
                        </g>
                    </svg>
                    </span>

                    <span class="pull-right">
                        <span class="navbar-toggle__text">Home</span>
                    </span>
                </a>
                <?php } ?>
            </div>

            <div class="collapse navbar-collapse" id="main-menu">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>

                    <span class="pull-left">
                        <span class="icon-bar icon-bar--first"></span>
                        <span class="icon-bar icon-bar--left"></span>
                        <span class="icon-bar icon-bar--right"></span>
                        <span class="icon-bar icon-bar--last"></span>
                    </span>
                </button>

                <ul class="languages">
                    <?php foreach (pll_languages_list(array('fields' => '')) as $lang) { ?>
                            <li class="languages__item">
                                <a href="/<?php print $lang -> slug; ?>"
                                   class="languages__link<?php pll_current_language() == $lang -> slug ? print ' languages__link--active' : false; ?>">
                                    <?php print $lang -> slug; ?>
                                </a>
                            </li>
                    <?php } ?>
                </ul>

                <div class="scrollspy">
                    <ul class="nav navbar-nav">
                        <li class="navbar-nav--hidden"><a href="#intro" title="<?php pll_e('Home'); ?>"><span><?php pll_e('Home'); ?></span></a></li>
                        <li><a href="<?php print $exeq_prefix; ?>#services" title="<?php pll_e('Usługi'); ?>"><span><?php pll_e('Usługi'); ?></span></a></li>
                        <li><a href="<?php print $exeq_prefix; ?>#process" title="<?php pll_e('Proces'); ?>"><span><?php pll_e('Proces'); ?></span></a></li>
                        <li><a href="<?php print $exeq_prefix; ?>#why-us" title="<?php pll_e('Dlaczego my?'); ?>"><span><?php pll_e('Dlaczego my?'); ?></span></a></li>
                        <li><a href="<?php print $exeq_prefix; ?>#clients" title="<?php pll_e('Klienci'); ?>"><span><?php pll_e('Klienci'); ?></span></a></li>
                        <li><a href="<?php print $exeq_prefix; ?>#know-how" title="<?php pll_e('Know how'); ?>"><span><?php pll_e('Know how'); ?></span></a></li>
                        <li><a href="<?php print $exeq_prefix; ?>#contact" title="<?php pll_e('Kontakt'); ?>"><span><?php pll_e('Kontakt'); ?></span></a></li>
                        <?php $url = get_category(
                            pll_get_term(
                                get_category_by_slug('dolacz-do-nas') -> term_id
                            )
                        ) -> slug; ?>
                        <li><a href="<?php print $exeq_prefix; ?><?php print $url; ?>"><span><?php pll_e('Dołącz do nas'); ?></span></a></li>
                    </ul>
                </div>

                <ul class="social-links">
                    <li class="social-links__item">
                        <a href="#" class="social-links__link">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 95.5 96" xml:space="preserve">
                                <path d="M48,0C21.5,0,0,21.5,0,48s21.5,48,48,48s48-21.5,48-48S74.5,0,48,0z M59.4,33.2h-7.2c-0.9,0-1.8,1.1-1.8,2.6V41h9v7.4h-9v22.3h-8.5V48.4h-7.7V41h7.7v-4.4c0-6.3,4.4-11.4,10.3-11.4h7.2L59.4,33.2L59.4,33.2L59.4,33.2z"/>
                            </svg>
                            <span class="sr-only">Facebook</span>
                        </a>
                    </li>

                    <li class="social-links__item">
                        <a href="#" class="social-links__link">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 95.5 96" xml:space="preserve">
                                <path d="M48,0C21.5,0,0,21.5,0,48s21.5,48,48,48s48-21.5,48-48S74.5,0,48,0z M67.5,39.3c0,0.4,0,0.8,0,1.2c0,12.5-9.5,26.9-26.9,26.9c-5.1,0-10.2-1.5-14.5-4.2c0.7,0.1,1.5,0.1,2.3,0.1c4.3,0,8.4-1.4,11.7-4c-4-0.1-7.6-2.7-8.8-6.6c0.6,0.1,1.2,0.2,1.8,0.2c0.8,0,1.7-0.1,2.5-0.3c-4.4-0.9-7.6-4.8-7.6-9.3c0,0,0-0.1,0-0.1c1.3,0.7,2.8,1.1,4.3,1.2c-4.2-2.8-5.4-8.3-2.9-12.6c4.8,5.9,11.9,9.5,19.5,9.9c-1.2-5.1,2-10.2,7.1-11.4c3.3-0.8,6.7,0.3,9,2.7c2.1-0.4,4.1-1.2,6-2.3c-0.7,2.2-2.2,4-4.2,5.2c1.9-0.2,3.7-0.7,5.4-1.5C71,36.3,69.4,38,67.5,39.3z"/>
                            </svg>
                            <span class="sr-only">Twitter</span>
                        </a>
                    </li>

                    <li class="social-links__item">
                        <a href="#" class="social-links__link">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 95.5 96" style="enable-background:new 0 0 95.5 96;" xml:space="preserve">
                                <g>
                                    <path d="M48.1,1.5C21.9,1.5,0.7,22.7,0.7,48.9s21.2,47.4,47.4,47.4s47.4-21.2,47.4-47.4S74.3,1.5,48.1,1.5z M40,66.4c0,0.4-0.9,0.6-1.3,0.6h-8.2c-0.4,0-0.5-0.1-0.5-0.6V40.2c0-0.4,0.1-0.2,0.5-0.2h8.2c0.4,0,1.3-0.3,1.3,0.1V66.4z M34.6,37.2c-3,0-5.4-2.4-5.4-5.4c0-3,2.4-5.4,5.4-5.4c3,0,5.4,2.4,5.4,5.4C40.1,34.8,37.6,37.2,34.6,37.2z M71,66.4c0,0.4-1.3,0.6-1.7,0.6h-8.2c-0.4,0-0.1-0.2-0.1-0.6V53.6c0-3.8-0.4-6.3-3.8-6.3c-3.4,0-4.2,2.3-4.2,6.1v13c0,0.4-0.6,0.6-1,0.6h-8.2c-0.4,0-0.8-0.2-0.8-0.6V40.1c0-0.4,0.4-0.1,0.8-0.1h7.9c0.4,0,0.3-0.3,0.3,0.1v1.4c3-1.5,4.9-2.4,7.9-2.4C69.1,39.1,71,46,71,52V66.4z"/>
                                </g>
                            </svg>
                            <span class="sr-only">LinkedIn</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
