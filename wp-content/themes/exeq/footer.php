<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <footer id="contact" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-1 col-md-3 col-md-offset-2">
                    <h3 class="footer__title"><?php pll_e('Obsługa klientów i kontakt dla mediów'); ?></h3>

                    <?php get_page_custom('Kontakt lewy'); ?>
                </div>

                <div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-1 col-md-3 col-md-offset-0 footer--offset">
                    <h3 class="footer__title"><?php pll_e('Dane do faktur i przelewów'); ?></h3>

                    <?php get_page_custom('Kontakt środek'); ?>
                </div>

                <div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-1 col-md-2 col-md-offset-0">
                    <h3 class="footer__title"><?php pll_e('Znajdziesz nas również na'); ?></h3>

                    <?php get_page_custom('Kontakt prawy'); ?>
                </div>

                <div class="col-xs-8 col-xs-offset-2 col-sm-12 col-sm-offset-0 footer__copy">
                    <hr>

                    <?php get_page_custom('Kontakt dół'); ?>
                </div>
            </div>
        </div>
    </footer>
</div>
<img class="hidden wait-for" src="<?php echo get_template_directory_uri(); ?>/img/h1-bg.jpg" alt="">
<div class="visible-md"></div>
<div class="visible-lg"></div>

<?php wp_footer(); ?>
</body>
</html>
